//
//  NetworkManager.swift
//  Slide for Reddit
//


import Foundation
import SDWebImage
import Alamofire
import BolinaInterceptor

class NetworkManagers {
    
    let alamofireManager: Alamofire.SessionManager!
    let sdWebImageDownloader: SDWebImageDownloader!

    static let shared = NetworkManagers()
    
    private init(){
        let bolinaSessionConfig = URLSessionConfiguration.default
        if bolinaSessionConfig.protocolClasses != nil {
            bolinaSessionConfig.protocolClasses!.insert(CdvlURLProtocol.self, at: 0)
        }
                    
        alamofireManager = Alamofire.SessionManager(configuration: bolinaSessionConfig)
                    
        let sdWebImageDownloaderConfig = SDWebImageDownloaderConfig.default
        sdWebImageDownloaderConfig.sessionConfiguration = bolinaSessionConfig
        sdWebImageDownloader = SDWebImageDownloader.init(config: sdWebImageDownloaderConfig)

        
    }
}
