### Codavel SDK ###

The purpose of this project is to demonstrate how easy is to integrate Codavel SDK into an Android app, by modifying Slide-iOS, a powerful open-source, ad-free, Swift-based Reddit browser for iOS.

You can check the original README [here](README.original.md), or directly in the original repository [here](https://github.com/ccrama/Slide-iOS).


### Integration ###

In order to integrate Codavel SDK, the following modifications to the original project were required (you can check all the code changes [here](https://gitlab.com/codavel/Slide-iOS/-/compare/develop...codavel?from_project_id=29761738)):

1. Include Codavel's dependency into the applications' Podfile, required to download and use Codavel's SDK
2. Start Codavel's Service with Codavel's Application ID and Secret when the app starts, by changing the didFinishLaunchingWithOptions method in the AppDelegate.
3. Created shared managers for SDWebImage and Alamofire, using a singleton pattern.
4. Replaced all usages of default shared manager of Alamofire and SDWebImage withour own shared manager, so that all the HTTP requests executed through the app's are processed and forwarded to our SDK.
